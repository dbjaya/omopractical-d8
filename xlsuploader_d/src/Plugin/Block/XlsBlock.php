<?php
namespace Drupal\xlsuploader_d\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
/**
 * Provides a block popuated by the excel data.
 *
 * @Block(
 *   id = "xls_block",
 *   admin_label = @Translation("xls Block"),
 *   category = @Translation("Custom article block example")
 * )
 */
class XlsBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $markup = "<h2>Colors taxonomy list</h2>";
    $markup .= "<ul>";
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('colors_list');
      foreach ($terms as $term) {    
        $markup .= "<li>".$term->name."</li>";
      }
    $markup .= "</ul>";
    return [
    '#markup' => $markup,
    ];  
  }
  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['xls_block_settings'] = $form_state->getValue('xls_block_settings');
  }
}