<?php

namespace Drupal\xlsuploader_d\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
/**
 *
 * @FieldWidget(
 *   id = "taxo_field_widget_default",
 *   label = @Translation("Custom taxo field widget"),
 *   field_types = {
 *     "taxo_field_item",
 *   }
 * )
 */
class TaxoFieldDefaultWidget extends WidgetBase {
  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('colors_list');
    $term_data = array();
    foreach ($terms as $term) {
      $term_data[$term->tid] = $term->name;
    }

    $element['value'] = $element + array(
        '#type' => 'select',
        '#options' => $term_data,
        '#empty_value' => '',
        '#default_value' => (isset($items[$delta]->value) && isset($term_data[$items[$delta]->value])) ? $items[$delta]->value : NULL,
        '#description' => t('Select a color'),
      );
    return $element;
  }



}
