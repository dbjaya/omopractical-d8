<?php
namespace Drupal\xlsuploader_d\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
/**
 *
 * @FieldType(
 *   id = "taxo_field_item",
 *   label = @Translation("Custom taxonomy field"),
 *   description = @Translation("Custom taxo field."),
 *   default_widget = "taxo_field_widget_default",
 *   default_formatter = "taxo_field_format_default",
 * )
 */
class TaxoFieldItem extends FieldItemBase {
  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Color'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return array(
      'columns' => array(
        'team_color' => array(
          'type' => 'varchar',
          'length' => 256,
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('team_color')->getValue();
    return $value === NULL || $value === '';
  }
}