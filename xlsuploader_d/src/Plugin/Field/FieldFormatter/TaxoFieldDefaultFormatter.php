<?php
namespace Drupal\xlsuploader_d\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 *
 * @FieldFormatter(
 *   id = "taxo_field_format_default",
 *   label = @Translation("Custom taxo field formatter"),
 *   field_types = {
 *     "taxo_field_item",
 *   }
 * )
 */
class TaxoFieldDefaultFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $elements = array();
    $colors = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('colors_list');
    foreach ($items as $delta => $item) {
      if (isset($colors[$item->value])) {
        $elements[$delta] = array('#markup' => $colors[$item->value]);
      }
    }
    return $elements;
  }
  
}
