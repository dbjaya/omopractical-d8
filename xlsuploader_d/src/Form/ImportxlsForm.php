<?php
/**
 * @file
 * Contains \Drupal\xlsuploader_d\ImportxlsForm.
 */

namespace Drupal\xlsuploader_d\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Symfony\Component\HttpFoundation\Response;
use \Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

class ImportxlsForm extends FormBase {

  function getFormId() {
    return 'xls_import_Form';
  }

  function buildForm(array $form, FormStateInterface $form_state) {
    $form = array(
      '#attributes' => array('enctype' => 'multipart/form-data'),
    );
    
    $form['file_upload_details'] = array(
      '#markup' => t('<b>Upload excel file here</b>'),
    );
  
    $validators = array(
      'file_validate_extensions' => array('xlsx'),
    );
    $form['xls_file'] = array(
      '#type' => 'managed_file',
      '#name' => 'xls_file',
      '#title' => t(''),
      '#size' => 20,
      '#description' => t('Excel format only'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://excel/',
    );
    
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {  
    if ($form_state->getValue('xls_file') == NULL) {
      $form_state->setErrorByName('xls_file', $this->t('upload proper File'));
    }
  }

  function submitForm(array &$form, FormStateInterface $form_state) {

    $file = \Drupal::entityTypeManager()->getStorage('file')->load($form_state->getValue('xls_file')[0]);
    $full_path = $file->get('uri')->value;
    $file_name = basename($full_path);
    try{
	    $inputFileName = \Drupal::service('file_system')->realpath('public://excel/'.$file_name);
	    $spreadsheet = IOFactory::load($inputFileName);
	    $sheetData = $spreadsheet->getActiveSheet();
	    $rows = array();

	    foreach ($sheetData->getRowIterator() as $row) {
	      $cellIterator = $row->getCellIterator();
	      $cellIterator->setIterateOnlyExistingCells(FALSE); 
	      $cells = [];
		      foreach ($cellIterator as $cell) {
		      $cells[] = $cell->getValue();
		      }
		      $rows[] = $cells;
	    }

	    array_shift($rows); // removes header
	    $vid = "colors_list";
	    $vocabularies = \Drupal\taxonomy\Entity\Vocabulary::loadMultiple();

	    if (!isset($vocabularies[$vid])) {
		$vocabulary = \Drupal\taxonomy\Entity\Vocabulary::create(array(
		    'vid' => $vid,
		    'description' => '',
		    'name' => "Team Colors",
		));
		$vocabulary->save();     
	    } else {
	      // $query = \Drupal::entityQuery('taxonomy_term');
	      // $query->condition('vid', $vid);
	      // $tids = $query->execute();
	      // $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
	      // $term_entities = $term_storage->loadMultiple($tids);
	      // $term_storage->delete($term_entities);
	    }
			foreach($rows as $row){
		        if(!empty($row[1])){			
				$termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
				$term = $termStorage->loadByProperties(['name' => $row[1], 'vid' => $vid]);
					if(empty($term)){
					  $termStorage->create([ 'vid' => $vid, 'name' => $row[1] ])->save();			
					} else {
						$first_match = reset($term);
						$first_match->name = $row[1];
						$first_match->save();
					}
				}
			}
			\Drupal::messenger()->addMessage('imported successfully');
			// $block = \Drupal\block_content\Entity\BlockContent::load('xls_block');
			// $blockview = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block);
			// print render($blockview);
    }catch (Exception $e) {
	    \Drupal::logger('type')->error($e->getMessage());
    } 
  }
}
